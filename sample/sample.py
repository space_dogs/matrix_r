import numpy as np

file = open ("matriz_R.csv", "w")
matriz_01 = np.genfromtxt ("matriz_01.csv", dtype = int, delimiter = ',')
matriz_02 = np.genfromtxt ("matriz_02.csv", dtype = int, delimiter = ',')

i01 = int (len (matriz_01)); j01 = int (len (matriz_01[0]))
i02 = int (len (matriz_02)); j02 = int (len (matriz_02[0]))

matriz_R = []

for a in range (i01):
    matriz_R.append([])
    for b in range (j02):
        for c in range (j01):
            adder = int (0)
            for d in range (i02):
                adder = adder + (matriz_01[a][d] * matriz_02[d][b])
        file.write (str(adder)+",")
    file.write ("\n")
    
file.close ()
